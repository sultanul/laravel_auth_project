@extends('layouts.app')

@section('content')




  <div class="row col-md-12 col-lg-12 col-sm-12" style="background: white; margin: 15px;">

    <form method="post" action="{{ route('companies.update',[$company->id])}}">
             {{ csrf_field() }}
      <input type="hidden" name="_method" value="put">
      <div class="form-group">
        <label for="company-name">Name<span class="required">*</span></label>
        <input class="form-control" id="company-name" required name="name" spellcheck="false" placeholder="Enter Name" value="{{$company->name}}" />
        
      </div>


      <div class="form-group">
        <label for="company-content">Description</label>
        <textarea placeholder="Enter Description" style="resize: vertical;" id="company-content" name="description" rows="5" spellcheck="false" class="form-control autosize-target text-left"> 
        {{ $company->description}}</textarea>
        
      </div>
      
      <button type="submit" class="btn btn-primary">Submit</button>
    </form>

  </div>


<div class="col-md-3 col-lg-3 col-sm-3 pull-right">
  <div class="sidebar-module">
    <h4>Action</h4>
    <ol class="list-unstyled">
      <li><a href="/companies/{{ $company->id }}">view Companies</a></li>
      <li><a href="/companies/">All Companies</a></li>
      
    </ol>
  </div>
</div>


@endsection