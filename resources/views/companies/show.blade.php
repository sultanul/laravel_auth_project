 @extends('layouts.app')

 @section('content')

    <div class="col-md-9 col-lg-9 col-sm-9 pull-left">

     <div class="jumbotron">
          <h1>{{ $company->name }}</h1>
          <p class="lead">{{ $company->description }}</p>
    
      </div>

     <div class="row col-md-12 col-lg-12 col-sm-12" style="background: white; margin: 10px;">
        @foreach($company->projects as $project)

           <div class="col-lg-4 col-md-4 col-sm-4">

                  <h2>{{ $project->name }}</h2>
                  <p class="text-danger">{{ $project->description }}</p>
                  <p><a class="btn btn-primary" href="/projects/{{ $project->id}}" role="button">View Project »</a></p>

           </div>

        @endforeach
      </div>
   </div>

    <div class="col-md-3 col-lg-3 col-sm-3 pull-right">
       <div class="sidebar-module">
           <h4>Action</h4> 
           <ol class="list-unstyled">
             <li><a href="/companies/{{ $company->id }}/edit">Edit</a></li>
             <li><a href="#">Delete</a></li>
             <li><a href="#">Add new member</a></li>
           </ol>
       </div>
     </div>


 @endsection